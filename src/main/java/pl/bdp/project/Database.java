package pl.bdp.project;

import pl.bdp.project.model.*;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class Database {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Database.class.getName());


    /**
     * Entity manager.
     */
    @PersistenceContext
    private EntityManager em;

    /**
     * Getting departments from database.
     * @return all departments.
     */
    public List<Department> getDepartments() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        query.from(Department.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *  Getting depots from database.
     * @return All depots.
     */
    public List<Depot> getDepots() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Depot> query = cb.createQuery(Depot.class);
        query.from(Depot.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Getting categories from database.
     * @return All categories.
     */
    public List<Category> getCategories() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        query.from(Category.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Getting items from database.
     * @return All items.
     */
    public List<Item> getItems() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        query.from(Item.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Adding depot to database.
     * @param depot
     */
    public void addDepot(Depot depot) {
        em.persist(depot);
    }

    /**
     * Adding department to database.
     * @param department
     */
    public void addDepartment(Department department) {
        em.persist(department);
    }

    /**
     * Remove single department from database.
     * @param department
     */
    public void removeDepartment(Department department) {
        em.remove(em.merge(department));
    }

    /**
     * Updating department in database.
     * @param department
     */
    public void setDepartment(Department department){
        em.merge(department);
    }

    /**
     * Updating tag in database
     * @param tag
     */
    public void setTag(Tag tag){
        em.merge(tag);
    }

    /**
     * Adding a new category to database.
     * @param category
     */
    public void addCategory(Category category){
        em.persist(category);
    }

    /**
     * Remove single depot from database.
     * @param depot
     */
    public void removeDepot(Depot depot) {
        em.remove(em.merge(depot));
    }

    /**
     * Updating depot in database.
     * @param depot
     */
    public void setDepot(Depot depot){
        em.merge(depot);
    }

    /**
     * Remove single category from database.
     * @param category
     */
    public void removeCategory(Category category) {
        em.remove(em.merge(category));
    }

    /**
     * Remove single tag from database.
      * @param tag
     */
    public void removeTag(Tag tag){
        em.remove(em.merge(tag));
    }

    /**
     * Adding a new tag to database.
     * @param tag
     */
    public void addTag(Tag tag){
        em.persist(tag);
    }

    /**
     * Getting list of tags from database.
     * @return List of tags.
     */
    public List<Tag> getTags() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        query.from(Tag.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Add single item to database.
     * @param item
     */
    public void addItem(Item item) {
        em.persist(item);
    }

    /**
     * Remove single item from database.
     * @param item
     */
    public void removeItem(Item item) {
        em.remove(em.merge(item));
    }

    /**
     * Getting users from database.
     * @return List of users.
     */
    public List<User> getUsers() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Add new user to database.
     * @param user
     */
    public void addUser(User user) {
        em.persist(user);
    }

    /**
     * Updating user in database.
     * @param user
     */
    public void setUser(User user){
        em.merge(user);
    }

    /**
     * Remove single user from database.
     * @param user
     */
    public void removeUser(User user) {
        em.remove(em.merge(user));
    }

    /**
     *
     * @param id Id of category to be returned.
     * @return Single category.
     */
    public Category getCategory(int id) {
        return em.find(Category.class, id);
    }

    /**
     * Updating category in database.
     * @param category
     */
    public void setCategory(Category category){
        em.merge(category);
    }

    /**
     * Find one department.
     * @param id of department to be returned
     * @return single department
     */
    public Department getDepartment(int id) {
        return em.find(Department.class, id);
    }

    /**
     * Find one tag.
     * @param id of tag to be returned
     * @return single tag
     */
    public Tag getTag(int id) {
        return em.find(Tag.class, id);
    }

    /**
     * Find one depot.
     * @param id of depot to be returned
     * @return single depot
     */
    public Depot getDepot(int id) {
        return em.find(Depot.class, id);
    }

    /**
     * Getting items that contain given tag.
     * @param tag
     * @return List of items with tag.
     */
    public List<Item> getItemsWhenGivenTag(Tag tag) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> item = query.from(Item.class);
        query.where(cb.isMember(tag, item.get(Item_.tags)));
        return em.createQuery(query).getResultList();
    }

    /**
     * Getting items that contain given department.
     * @param department
     * @return List of items with department.
     */
    public List<Item> getItemsWhenGivenDepartment(Department department) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> item = query.from(Item.class);
        query.where(cb.equal(item.get(Item_.department), department));
//        em.createQuery(query).getResultList().forEach(a -> System.out.println(a.getName()));
        return em.createQuery(query).getResultList();
    }

    /**
     * Getting items that contain given depot.
     * @param depot
     * @return List of items with depot.
     */
    public List<Item> getItemsWhenGivenDepot(Depot depot) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> item = query.from(Item.class);
        query.where(cb.equal(item.get(Item_.depot), depot));
        return em.createQuery(query).getResultList();
    }

    /**
     * Getting items that contain given category.
     * @param category
     * @return List of items with category.
     */
    public List<Item> getItemsWhenGivenCategory(Category category) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> item = query.from(Item.class);
        query.where(cb.equal(item.get(Item_.category), category));
        return em.createQuery(query).getResultList();
    }

    /**
     * @param id of single item.
     * @return Item from database with id passed as parameter.
     */
    public Item getItem(int id) {
        Item item = em.find(Item.class, id);
        em.detach(item);
        item.setTags(new ArrayList<>(item.getTags()));
        return item;
    }

    /**
     * Saves updated item to database.
     * @param item
     */
    public void updateItem(Item item) {
        em.merge(item);
    }
}
