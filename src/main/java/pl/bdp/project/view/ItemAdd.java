package pl.bdp.project.view;

import pl.bdp.project.Database;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Bean for adding new item.
 */
@Named
@ViewScoped
public class ItemAdd implements Serializable {

    @EJB
    private Database database;

    @Inject
    private ItemsView itemsView;

    private Item item = new Item();

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void addItem() {
        database.addItem(item);
        itemsView.getItems().add(item);
        item = new Item();
    }

}
