package pl.bdp.project.view;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.User;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class UsersView implements Serializable {


    /**
     * Injecting database.
     */
    @EJB
    private Database database;

    /**
     * Injecting RoleView.
     */
    @Inject
    private RoleView roleView;

    private User user = new User();

    /**
     * List of users to display.
     */
    private List<User> usersList;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Lazy loading for list of users.
     *
     * @return list of all users.
     */
    public List<User> getUsers() {
        if (usersList == null) {
            usersList = database.getUsers();
        }
        return usersList;
    }

//    /**
//     * Add new user only if there is no department with given name else prints error message.
//     *
//     * @param login    must be unique
//     * @param name
//     * @param surname
//     * @param password
//     * @param role
//     */
//    public void addUser(String login, String name, String surname, String password, User.Role role) {
//        long exist = database.getUsers().stream().filter(value -> login.equals(value.getLogin())).count();
//        if (exist == 0) {
//            database.addUser(new User(login, name, surname, DigestUtils.sha256Hex(password), role));
//            usersList = null;
//        } else {
//            addMessage("Adding user", "Nie mozna dodać użytkownika o takim loginie. Login zajęty.");
//        }
//    }

    /**
     * Removes single user from database
     *
     * @param user
     */
    public void removeUser(User user) {
        if (roleView.isLoggedIn(user)) {
            addMessage("Error", "Unable to delete self.");
        } else {
            database.removeUser(user);
            usersList = null;
        }
    }

    /**
     * Primefaces method to show message.
     *
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Changes cell value when edited on website.
     *
     * @param event
     */
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            DataTable dataTable = (DataTable) event.getSource();
            User user = (User) dataTable.getRowData();
            boolean exists = false;
            for (User userInDb : database.getUsers()) {
                if (userInDb.getLogin().equalsIgnoreCase(user.getLogin())) {
                    addMessage("Error", "This login is already in use.");
                    exists = true;
                }
            }
            if (!exists) {
                database.setUser(user);
                addMessage("Value changed", "");
            }
        }
        usersList = null;
    }

    /**
     * Method adding a new User. If login already exists in database, a message is shown.
     */
    public void addUser() {
        boolean unique = true;
        for (User isUser : database.getUsers()) {
            if (isUser.getLogin().equalsIgnoreCase(user.getLogin())) {
                addMessage("Error", "This login is already in use.");
                unique = false;
            }
        }
        if (unique) {
            database.addUser(user);
        }
        usersList = null;
        user = new User();
    }
}
