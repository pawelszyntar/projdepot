package pl.bdp.project.view;

import pl.bdp.project.model.User;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@ViewScoped
@Named
public class RoleView implements Serializable {

    /**
     *
     * @return whether current user is admin.
     */
    public boolean isAdmin() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole(User.Role.ADMIN.toString());
    }

    /**
     *
     * @return whether current user is admin.
     */
    public boolean isUser() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole(User.Role.USER.toString());
    }

    /**
     *
     * @param user
     * @return whether user is logged in.
     */
    public boolean isLoggedIn(User user) {
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser().equals(user.getLogin());
    }

    /**
     * Check user.
     * @return current user's name.
     */
    public String getUsername() {
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }

}
