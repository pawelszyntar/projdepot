package pl.bdp.project.view;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

@Named
@ViewScoped
public class ItemEdit implements Serializable {

    /**
     * Injecting database.
     */
    @EJB
    private Database database;

    private int itemId;

    public void setItem(Item item) {
        this.item = item;
    }

    private Item item;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     * Get edited item.
     * @return this item.
     */
    public Item getItem() {
        if (item == null) {
            item = database.getItem(itemId);
        }
        return item;
    }

    /**
     * Save edited item.
     */
    public void saveItem() {
        database.updateItem(item);
    }

    /**
     * Edit item's image.
     * @param event from primefaces Uploadfile method.
     */
    public void edit (FileUploadEvent event) {

        byte[] edit;

        try {
            InputStream inputStream = event.getFile().getInputstream();
            edit = IOUtils.toByteArray(inputStream);
            item.setImage(edit);
        } catch (IOException h) {
            h.printStackTrace();
        }
        FacesMessage msg = new FacesMessage("File Description", "File Uploaded");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
