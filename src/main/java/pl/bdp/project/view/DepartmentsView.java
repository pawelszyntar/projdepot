package pl.bdp.project.view;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Department;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class DepartmentsView implements Serializable {

    /**
     * Injecting database.
     */
    @EJB
    private Database database;

    private Department department = new Department();

    /**
     * List of departments to display.
     */
    private List<Department> departmentList;

    /**
     * Selectable list of departments for Primefaces.
     */
    private List<SelectItem> departmentsToSelect;

    /**
     * Lazy loading for list of departments.
     * @return all departments.
     */
    public List<Department> getDepartments() {
        if (departmentList == null){
            departmentList = database.getDepartments();
        }
        return departmentList;
    }

    /**
     * Creates list of SelectItem witch contains department and its name.
     * @return list of available departments.
     */
    public List<SelectItem> getDepartmentsToSelect() {
        if (departmentsToSelect == null) {
            departmentsToSelect = new ArrayList<>();
            database.getDepartments().forEach(dep -> {
                departmentsToSelect.add(new SelectItem(dep, dep.getName()));
            });
        }
        return departmentsToSelect;
    }

    /**
     * Add new department with name and reloads page.
     */
    public void addDepartment() {
        boolean exist = false;
        for (Department used : database.getDepartments()) {
            if (used.getName().equalsIgnoreCase(department.getName())){
                addMessage("Error", "Department already exists.");
                exist = true;
            }
        }
        if (!exist) {
            database.addDepartment(department);
        }
        departmentList = null;
        department = new Department();
    }

    /**
     * Removes single department from database.
     * @param department
     */
    public void removeDepartment(Department department) {

        List<Item> listOfItemsDeleting = new ArrayList<>(database.getItemsWhenGivenDepartment(department));

        if (database.getItemsWhenGivenDepartment(department).isEmpty()){
            database.removeDepartment(department);
        } else {

            String listOfItems = "";
            for (Item item:listOfItemsDeleting) {
                listOfItems = listOfItems + " " + item.getName();
            }
            addMessage("Error", "Department assigned to item(s). List of items: " + listOfItems);
        }
        departmentList = null;
    }

    /**
     * Primefaces method to show message.
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);

    }

    /**
     * Changes cell value when edited on website.
     * @param event
     */
    public void onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable dataTable = (DataTable)event.getSource();
            Department department = (Department)dataTable.getRowData();
            database.setDepartment(department);

            addMessage( "Name changed", "Old: " + oldValue + ", New:" + newValue);
        }
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
