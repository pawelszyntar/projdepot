package pl.bdp.project.view;


import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import java.io.*;

@Named
@ApplicationScoped
public class ImageDisplay {

    /**
     * Inject database.
     */
    @EJB
    private Database data;

    /**
     * To prevent errors when image is null in database.
     */
    String noImage = "NO-IMAGE-TO-DISPLAY";

    /**
     * Displays image from database using Primefaces.
     * @return
     */
    public StreamedContent getMyImage() {

        StreamedContent stream;
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            stream = new DefaultStreamedContent();
        } else {
            String itemId = fc.getExternalContext().getRequestParameterMap().get("itemId");
            Item imageItem = data.getItem(Integer.parseInt(itemId));
            if (imageItem.getImage() != null) {
                byte[] byteArray = imageItem.getImage();
                stream = new DefaultStreamedContent(new ByteArrayInputStream(byteArray));
            } else stream = new DefaultStreamedContent(new ByteArrayInputStream(noImage.getBytes()));
        }
        return stream;
    }
}
