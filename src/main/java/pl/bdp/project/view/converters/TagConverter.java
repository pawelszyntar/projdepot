package pl.bdp.project.view.converters;

import pl.bdp.project.Database;
import pl.bdp.project.model.Tag;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("tagConverter")
public class TagConverter implements Converter {

    /**
     * Converter for Tag class.
     */

    /**
     * Converts tag to object used in jsf beans.
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Database database = CDI.current().select(Database.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return database.getTag(Integer.parseInt(value));
    }

    /**
     * Converts tag to String used in html forms.
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Tag) value).getId() + "";
    }
}
