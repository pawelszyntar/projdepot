package pl.bdp.project.view.converters;

import pl.bdp.project.Database;
import pl.bdp.project.model.Department;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("DepartmentConverter")
public class DepartmentConverter implements Converter {

    /**
     * Converter for Department class.
     */

    /**
     * Converts department to object used in jsf beans.
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Database database = CDI.current().select(Database.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return database.getDepartment(Integer.parseInt(value));
    }

    /**
     * Converts department to String used in html forms.
     * @param context
     * @param component
     * @param department
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object department) {
        if (department == null) {
            return "null";
        }
        return ((Department) department).getId() + "";
    }
}
