package pl.bdp.project.view.converters;

import pl.bdp.project.Database;
import pl.bdp.project.model.Depot;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("depotsConverter")
public class DepotsConverter implements Converter {

    /**
     * Converter for Depot class.
     */

    /**
     * Converts depot to object used in jsf beans.
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Database database = CDI.current().select(Database.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return database.getDepot(Integer.parseInt(value));
    }

    /**
     * Converts depot to String used in html forms.
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Depot) value).getId() + "";
    }

}
