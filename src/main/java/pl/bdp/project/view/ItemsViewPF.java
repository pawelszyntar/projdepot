package pl.bdp.project.view;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Item;
import pl.bdp.project.model.Tag;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * First view with PrimeFaces
 *
 * @author DFL
 */
@Named("itemsviewpf")
@ViewScoped
public class ItemsViewPF implements Serializable {

    /**
     * Inject database.
     */
    @EJB
    private Database database;

    private Item item = new Item();

    public List<Item> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Item> selectedItems) {
        this.selectedItems = selectedItems;
    }

    private List<Item> selectedItems;

    /**
     * List of items.
     */
    private List<Item> items;

    /**
     * List of filtered items.
     */
    private List<Item> filteredItems;

    /**
     * Depots to select in filter
     */
    private List<SelectItem> depotsToSelect;

    /**
     * Departments to select in filter
     */
    private List<SelectItem> departmentsToSelect;

    /**
     * Categories to select in filter
     */
    private List<SelectItem> categoryToSelect;

    /**
     * Tags to select in filter
     */
    private List<SelectItem> tagsToSelect;

    /**
     * SelectItem list for primefaces.
     * @return list of available tags.
     */
    public List<SelectItem> getTagsToSelect() {
        if (tagsToSelect == null) {
            tagsToSelect = new ArrayList<>();
            database.getTags().forEach(tag -> {
                tagsToSelect.add(new SelectItem(tag, tag.getName()));
            });
        }
        return tagsToSelect;
    }

    /**
     * SelectItem list for primefaces.
     * @return list of available categories.
     */
    public List<SelectItem> getCategoryToSelect() {
        if (categoryToSelect == null) {
            categoryToSelect = new ArrayList<>();
            database.getCategories().forEach(category -> {
                categoryToSelect.add(new SelectItem(category, category.getName()));
            });
        }
        return categoryToSelect;
    }

    /**
     * SelectItem list for primefaces.
     * @return list of available departments.
     */
    public List<SelectItem> getDepartmentsToSelect() {
        if (departmentsToSelect == null) {
            departmentsToSelect = new ArrayList<>();
            database.getDepartments().forEach(department -> {
                departmentsToSelect.add(new SelectItem(department, department.getName()));
            });
        }
        return departmentsToSelect;
    }

    /**
     * SelectItem list for primefaces.
     * @return list of available depots.
     */
    public List<SelectItem> getDepotsToSelect() {
        if (depotsToSelect == null) {
            depotsToSelect = new ArrayList<>();
            database.getDepots().forEach(depot -> {
                depotsToSelect.add(new SelectItem(depot, depot.getName() + ""));
            });
        }
        return depotsToSelect;
    }

    /**
     * @return list of filtered items.
     */
    public List<Item> getFilteredItems() {

        return filteredItems;
    }

    /**
     * @param filteredItems setting of filtered items.
     */
    public void setFilteredItems(List<Item> filteredItems) {
        this.filteredItems = filteredItems;
    }

    /**
     * @return items from database.
     */
    public List<Item> getItems() {
        if (items == null) {
            items = database.getItems();
        }
        return items;
    }

    /**
     * Setting list of items.
     * @param items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }


    /**
     * Remove items
     * @param item
     */
    public void removeItem(Item item) {
        database.removeItem(item);
        items = null;
        filteredItems = null;
        // todo dodac sprawdzenie czy item nadal jest w bazie danych
        addMessage("Deleting item.", "Item delete confirmed.");

    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void addItem() {
        database.addItem(item);
        items = null;
        item = new Item();
    }

    /**
     * Upload image to database.
     * @param event from primefaces Uploadfile method.
     */
    public void upload (FileUploadEvent event) {

        byte[] image;

        try {
            InputStream inputStream = event.getFile().getInputstream();
            image = IOUtils.toByteArray(inputStream);
            item.setImage(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FacesMessage msg = new FacesMessage("File Description", "File Uploaded");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     *
     * @param value
     * @param filter
     * @param locale
     * @return if it does contain a given tag
     */
    public boolean filterByTag(Object value, Object filter, Locale locale) {

        boolean hasTag = false;

        //pobranie stringa zlozonego ze wszystkich tagow
        String tagsString = (String) value;

        //inicjalizacja listy wybranych tagow do filtrowania
        ArrayList<Tag> listOfTags = new ArrayList<>();

        //zrzutowanie kardzego elementu i dodanie go do listy mozliwych tagow
        for (Object o : (Object[]) filter) {

            listOfTags.add((Tag) o);
        }

        //sprawdzenie czy nie wybrano zadnego taga
        if (listOfTags.isEmpty() ) {
            hasTag = true;
        } else {

            //sprawdzenie czy string tagow zawiera tagi zaznaczone
            for (Tag tag : listOfTags) {
                if (tagsString.contains(tag.getName())) {
                    hasTag = true;
                }
            }
        }
        return hasTag;
    }

//    private List<Boolean> visibleColumn = Arrays.asList(true, true, true, true, true , true, true, true, true, true);
//
//    public List<Boolean> getVisibleColumn() {
//        return visibleColumn;
//    }
//
//    public void onToggle(ToggleEvent e) {
//        visibleColumn.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
//    }

    /**
     * Primefaces method to show message.
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);

    }

//    public void addItemForm(){
//        Map<String,Object> options = new HashMap<String, Object>();
//        options.put("resizable", false);
//        options.put("draggable", false);
//        options.put("modal", true);
//        RequestContext.getCurrentInstance().openDialog("itemadd", options, null);
//
//    }

    public void deleteSelectedItem(){

        for (Item selectedItem: selectedItems) {
            database.removeItem(selectedItem);
        }

        items = null;
        filteredItems = null;

    }
}

