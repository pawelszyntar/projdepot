package pl.bdp.project.view;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@RequestScoped
public class LoginBean implements Serializable {


    private String username;
    private String password;

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(LoginBean.class.getName());

    /**
     * Login method.
     * @return
     */
    public String login() {

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        try {
            request.login(username, password);
            return "/itemspf.xhtml?faces-redirect=true";
        } catch (ServletException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return "/error.xhtml?faces-redirect=true";
        }
    }

    /**
     * Logout method destroying current session.
     */
    public void logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
