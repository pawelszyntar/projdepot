package pl.bdp.project.view;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Item;
import pl.bdp.project.model.Tag;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Named
@ViewScoped
public class TagsView implements Serializable {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(DepartmentsView.class.getName());

    /**
     * Injecting database.
     */
    @EJB
    private Database database;

    private Tag tag = new Tag();

    /**
     * List of tags.
     */
    private List<Tag> tagList;

    /**
     * List of tags to select.
     */
    private List<SelectItem> tagsToSelect;

    /**
     *
     * @return list of all tags.
     */
    public List<Tag> getTagsList() {
        if (tagList == null){
            tagList = database.getTags();
        }
        return tagList;
    }

    /**
     * Adding tag with reload page.
     */
    public void addTag() {
        boolean exist = false;
        for (Tag used : database.getTags()) {
            if (used.getName().equalsIgnoreCase(tag.getName())){
                addMessage("Error", "Tag already exists.");
                exist = true;
            }
        }
        if (!exist) {
            database.addTag(tag);
        }
        tagList = null;
        tag = new Tag();
    }

    /**
     * Removes single tag.
     * @param tag
     */
    public void removeTag(Tag tag) {

        List<Item> listOfItemsDeleting = new ArrayList<>(database.getItemsWhenGivenTag(tag));

        if (database.getItemsWhenGivenTag(tag).isEmpty()) {
            database.removeTag(tag);
        } else {

            String listOfItems = "";
            for (Item item:listOfItemsDeleting) {
                listOfItems = listOfItems + " " + item.getName();
            }
            addMessage("Error", "Tag assigned to item(s). List of items: " + listOfItems);
        }
        tagList = null;
    }

    /**
     *
     * @return list of available tags
     */
    public List<SelectItem> getTags() {
        if (tagsToSelect == null) {
            tagsToSelect = new ArrayList<>();
            database.getTags().forEach(tag -> {
                tagsToSelect.add(new SelectItem(tag, tag.getName()));
            });
        }
        return tagsToSelect;
    }

    /**
     * Primefaces method to show message.
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);

    }

    /**
     * Changes cell value when edited on website.
     * @param event
     */
    public void onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable dataTable = (DataTable)event.getSource();
            Tag tag = (Tag)dataTable.getRowData();
            database.setTag(tag);

            addMessage( "Name changed", "Old: " + oldValue + ", New:" + newValue);
        }
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
