package pl.bdp.project.view;


import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Category;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Named
@ViewScoped
public class CategoriesView implements Serializable {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(DepartmentsView.class.getName());

    /**
     * Inject database.
     */
    @EJB
    private Database database;

    /**
     * List of all categories
     */
    private List<Category> categoryList;

    /**
     * List of categories to select.
     */
    private List<SelectItem> categoriesToSelect;

    private Category category = new Category();

    /**
     *
     * @return list of all categories.
     */
    public List<Category> getCategoryList() {
        if(categoryList==null){
            categoryList = database.getCategories();
        }
        return categoryList;
    }

    /**
     * Creates list of SelectItem witch contains category and its name.
     * @return list of available categories.
     */
    public List<SelectItem> getCategories() {
        if (categoriesToSelect == null) {
            categoriesToSelect = new ArrayList<>();
            database.getCategories().forEach(cat -> {
                categoriesToSelect.add(new SelectItem(cat, cat.getName()));
            });
        }
        return categoriesToSelect;
    }

    /**
     * Adding category with name and reloads page.
     */
    public void addCategory() {
        boolean exist = false;
        for (Category used : database.getCategories()) {
            if (used.getName().equalsIgnoreCase(category.getName())){
                addMessage("Error", "Category already exists.");
                exist = true;
            }
        }
        if (!exist) {
            database.addCategory(category);
        }
        categoryList = null;
        category = new Category();
    }

    /**
     * Removes single depot from database.
     * @param category
     */
    public void removeCategory(Category category) {

        List<Item> listOfItemsDeleting = new ArrayList<>(database.getItemsWhenGivenCategory(category));

        if (database.getItemsWhenGivenCategory(category).isEmpty()) {
            database.removeCategory(category);
        } else {
            String listOfItems = "";
            for (Item item:listOfItemsDeleting) {
                listOfItems = listOfItems + " " +  item.getName() + " ";
            }
            addMessage("Error", "Category assigned to item(s). List of items: " + listOfItems);
        }
        categoryList = null;
    }

    /**
     * Primefaces method to show message.
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Changes cell value when edited on website.
     * @param event
     */
    public void onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable dataTable = (DataTable)event.getSource();
            Category category = (Category)dataTable.getRowData();
            database.setCategory(category);

            addMessage( "Name changed", "Old: " + oldValue + ", New:" + newValue);
        }
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


}
