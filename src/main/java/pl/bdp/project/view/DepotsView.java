package pl.bdp.project.view;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import pl.bdp.project.Database;
import pl.bdp.project.model.Depot;
import pl.bdp.project.model.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class DepotsView implements Serializable {

    /**
     * Injecting database.
     */
    @EJB
    private Database database;

    private Depot depot = new Depot();

    /**
     * List of depots to display.
     */
    private List<Depot> depots;

    /**
     * List of available depots.
     */
    private List<SelectItem> selectedDepot;

    /**
     * @return All depots.
     */
    public List<Depot> getDepots() {
        if (depots == null) {
            depots = database.getDepots();
        }
        return depots;
    }

    /**
     * Creates list of SelectItem witch contains depot and its name.
     * @return list of available depots.
     */
    public List<SelectItem> getSelectedDepot() {
        if (selectedDepot == null) {
            selectedDepot = new ArrayList<>();
            database.getDepots().forEach(depot -> {
                selectedDepot.add(new SelectItem(depot, depot.getName() + ""));
            });
        }
        return selectedDepot;
    }

    /**
     * Adds new depot to database.
     */
    public void addDepot() {
        boolean exists = false;
        for (Depot isDepot : getDepots()) {
            if (isDepot.getName().equalsIgnoreCase(depot.getName())) {
                addMessage("Error", "Depot already exists.");
                exists = true;
            }
        }
        if (!exists) {
            database.addDepot(depot);
        }
        depots = null;
        depot = new Depot();
    }

    /**
     * Removes single depot from database.
     * @param depot
     */
    public void removeDepot(Depot depot) {

        List<Item> listOfItemsDeleting = new ArrayList<>(database.getItemsWhenGivenDepot(depot));

        if (database.getItemsWhenGivenDepot(depot).isEmpty()) {
            database.removeDepot(depot);
        } else {

            String listOfItems = "";
            for (Item item:listOfItemsDeleting) {
                listOfItems = listOfItems + " " + item.getName();
            }
            addMessage("Error", "Depot assigned to item(s). List of items: " + listOfItems);
        }
        depots = null;
    }

    /**
     * Primefaces method to show message.
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Changes cell value when edited on website.
     * @param event
     */
    public void onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable dataTable = (DataTable)event.getSource();
            Depot depot = (Depot)dataTable.getRowData();
            database.setDepot(depot);

            addMessage( "Name changed", "Old: " + oldValue + ", New:" + newValue);

        }
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }
}