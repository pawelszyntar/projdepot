package pl.bdp.project.view;

import pl.bdp.project.Database;
import pl.bdp.project.model.*;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class ItemsView implements Serializable {

    /**
     * Inject database.
     */
    @EJB
    private Database database;

    /**
     * List of items.
     */
    private List<Item> items;

    /**
     *
     * @return list of all items.
     */
    public List<Item> getItems() {
        if (items == null) {
            items = database.getItems();
        }
        return items;
    }

    public void removeItem(Item item) {
        database.removeItem(item);
        items = null;
    }

    public List<Item> getItemsGivenTag(Tag tag) {
        items = database.getItemsWhenGivenTag(tag);
        return items;
    }

    public List<Item> getItemsGivenDepartment(Department dep) {
        items = database.getItemsWhenGivenDepartment(dep);
        return items;
    }

}
