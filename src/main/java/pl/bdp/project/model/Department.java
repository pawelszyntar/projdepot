package pl.bdp.project.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "departments")
public class Department implements Serializable {

    /**
     * Tag Id for mysql, primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Department name for mysql
     */
    @Column//(unique = true)
    private String name;

    public Department(String name) {
        this.name = name;
    }

    public Department() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Defining comparison
     * @param o is object witch it will compare to.
     * @return whether object is equal to "o".
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    /**
     * Turns object to coded value.
     * @return unique integer.
     */
    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
