package pl.bdp.project.model;

import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class User implements Serializable {

    /**
     * Enum describing possible roles for user.
     */
    public enum Role {
        USER, ADMIN
    }

    /**
     * User Id for mysql, primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * User login for mysql.
     */
    @Column//(unique = true)
    private String login;

    /**
     * User name for mysql.
     */
    @Column
    private String name;

    /**
     * User surname for mysql.
     */
    @Column
    private String surname;

    /**
     * User password for mysql.
     */
    @Column
    private String password;

    /**
     * User role for mysql.
     */
    @Enumerated(EnumType.STRING)
    @Column
    private Role role;

    public User() {
    }

    public User(String login, String name, String surname, String password, Role role) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password = DigestUtils.sha256Hex(password);
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
