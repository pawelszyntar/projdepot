package pl.bdp.project.model;

import org.hibernate.type.Type;
import org.hibernate.type.descriptor.sql.LobTypeMappings;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "items")
public class Item implements Serializable {

    /**
     * Item id for mysql, primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Item name for mysql.
     */
    @Column
    private String name;

    /**
     * Item tag for mysql.
     * Many to many relation with tags table.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "items_tags", joinColumns = @JoinColumn(name = "item", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag", referencedColumnName = "id"))
    private List<Tag> tags = new ArrayList<>();

    /**
     * Item depot for mysql.
     * Many to one relation with depots table.
     */
    @ManyToOne
    @JoinColumn(name = "depot", referencedColumnName = "id")
    private Depot depot;

    /**
     * Item department for mysql.
     * Many to one relation with departments table.
     */
    @ManyToOne
    @JoinColumn(name = "department", referencedColumnName = "id")
    private Department department;

    /**
     * Item category for mysql.
     * Many to one relation with categories table.
     */
    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id")
    private Category category;

    /**
     * Item image for mysql.
     * Column type is blob.
     */
    @Column
    @Lob
    private byte[] image;

    /**
     * Item arrival date for mysql.
     */
    @Column(name = "arrival_date")
    private LocalDateTime date;

    public Item(String name, List<Tag> tags, Depot depot, Department department, Category category,
                byte[] image) {
        this.name = name;
        this.tags = tags;
        this.depot = depot;
        this.department = department;
        this.category = category;
        this.image = image;
        this.tagsToString = tagsString();
        this.date = LocalDateTime.now();
    }


    @Transient
    private String tagsToString;

    public String getTagsToString() {
        return tagsString();
    }

    public void setTagsToString(String tagsToString) {
        this.tagsToString = tagsToString;
    }

    /**
     *
     * @return list of tags by one string
     */
    public String tagsString(){

        String tagsString = "";

        for (Tag tag: this.tags) {
            tagsString = tagsString + " " + tag.getName();
        }
        return tagsString;
    }

    public Item() {
        this.date = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * Formats date to be easier to read.
     * @return formatted date.
     */
    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return date.format(formatter);
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
