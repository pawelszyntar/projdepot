package pl.bdp.project;


import pl.bdp.project.model.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Startup
public class DatabaseContext {

    /**
     * Entity manager
     */
    @PersistenceContext
    private EntityManager em;

    /**
     * Inserting data into database for development process only.
     */
    @PostConstruct
    public void init(){
        User admin = new User("admin", "", "", "admin", User.Role.ADMIN);
        User user = new User("user", "", "", "user", User.Role.USER);
        em.persist(admin);
        em.persist(user);
        Category category = new Category("Przyprawy");
        Category category1 = new Category("Nabial");
        Category category2 = new Category("Pieczywo");
        Category category3 = new Category("Alkohol");
        Category category4 = new Category("Inne");
        em.persist(category);
        em.persist(category1);
        em.persist(category2);
        em.persist(category3);
        em.persist(category4);
        Department department = new Department("Zywnosc");
        Department department1 = new Department("Obuwie");
        Department department2 = new Department("Odziez");
        Department department3 = new Department("Elektronika");
        em.persist(department);
        em.persist(department1);
        em.persist(department2);
        em.persist(department3);
        Depot depot = new Depot("Karczemki");
        Depot depot1 = new Depot("Matarnia");
        Depot depot2 = new Depot("Kokoszki");
        Depot depot3 = new Depot("Osowa");
        em.persist(depot);
        em.persist(depot1);
        em.persist(depot2);
        em.persist(depot3);
        Tag tag1 = new Tag("Tag1");
        Tag tag2 = new Tag("Tag2");
        Tag tag3 = new Tag("Tag3");
        Tag tag4 = new Tag("Tag4");
        em.persist(tag1);
        em.persist(tag2);
        em.persist(tag3);
        em.persist(tag4);
        List<Tag> tagList1 = new ArrayList<>();
        tagList1.add(tag1);
        tagList1.add(tag2);
        tagList1.add(tag3);
        tagList1.add(tag4);
        List<Tag> tagList2 = new ArrayList<>();
        tagList2.add(tag2);
        tagList2.add(tag4);
        List<Tag> tagList3 = new ArrayList<>();
        tagList3.add(tag1);
        tagList3.add(tag3);
        tagList3.add(tag4);
        List<Tag> tagList4 = new ArrayList<>();
        tagList4.add(tag1);
        Item item = new Item("mleko", tagList1, depot2, department, category1,null);
        Item item1 = new Item("chleb", tagList2, depot, department, category2, null);
        Item item2 = new Item("telewizor", tagList3, depot2, department3, category4, null);
        Item item3 = new Item("mikrofalowka", tagList4, depot1, department3, category4, null);
        em.persist(item);
        em.persist(item1);
        em.persist(item2);
        em.persist(item3);
    }


}
